import 'package:flutter/material.dart';

const Color logoColor = Color(0xFFE0F4FB);

const TextStyle kTextStyleProximaL = TextStyle(
  fontFamily: 'proximaL',
);

const TextStyle kTextStyleProximaR = TextStyle(
  fontFamily: 'proximaR',
);

const TextStyle kTextStyleProximaB = TextStyle(
  fontFamily: 'proximaB',
);

const TextStyle kTextStyleYellowMoon = TextStyle(
  fontFamily: 'YellowMoon',
);
