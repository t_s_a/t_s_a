# TSA_Gram

A new Flutter project.

## Getting Started

TSA_Gram is an Instagram like app for a Flutter Workshop.

## How to Use 

**Step 1:**

Download or clone this repo by using the link below:

```
https://gitlab.com/t_s_a/t_s_a.git
```

**Step 2:**

Go to project root and execute the following command in console to get the required dependencies: 

```
flutter pub get 
```

**Step 3:**

Run the project in your prefer plateform !
